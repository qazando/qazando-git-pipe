$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Login de Usuario",
  "description": "",
  "id": "login-de-usuario",
  "keyword": "Funcionalidade"
});
formatter.before({
  "duration": 9748783226,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-de-usuario;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 5,
      "name": "@test"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "que eu escreva as informações do usuário",
  "keyword": "Dado "
});
formatter.step({
  "line": 8,
  "name": "eu clicar em logar",
  "keyword": "Quando "
});
formatter.step({
  "line": 9,
  "name": "vou estar logado no app",
  "keyword": "Então "
});
formatter.match({
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário()"
});
formatter.result({
  "duration": 2034600599,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.eu_clicar_em_logar()"
});
formatter.result({
  "duration": 550875895,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.vou_estar_logado_no_app()"
});
formatter.result({
  "duration": 594754,
  "status": "passed"
});
formatter.before({
  "duration": 1029822069,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-de-usuario;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 11,
      "name": "@test"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "que eu escreva as informações do usuário",
  "keyword": "Dado "
});
formatter.step({
  "line": 14,
  "name": "eu clicar em logar",
  "keyword": "Quando "
});
formatter.step({
  "line": 15,
  "name": "vou estar logado no app",
  "keyword": "Então "
});
formatter.match({
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário()"
});
formatter.result({
  "duration": 2116714072,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.eu_clicar_em_logar()"
});
formatter.result({
  "duration": 544564300,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.vou_estar_logado_no_app()"
});
formatter.result({
  "duration": 38297,
  "status": "passed"
});
formatter.before({
  "duration": 1015234532,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-de-usuario;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 17,
      "name": "@test"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "que eu escreva as informações do usuário",
  "keyword": "Dado "
});
formatter.step({
  "line": 20,
  "name": "eu clicar em logar",
  "keyword": "Quando "
});
formatter.step({
  "line": 21,
  "name": "vou estar logado no app",
  "keyword": "Então "
});
formatter.match({
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário()"
});
formatter.result({
  "duration": 1608775949,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.eu_clicar_em_logar()"
});
formatter.result({
  "duration": 541126828,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.vou_estar_logado_no_app()"
});
formatter.result({
  "duration": 34614,
  "status": "passed"
});
});